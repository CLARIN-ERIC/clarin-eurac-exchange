#!/bin/bash
_MAX=100
_LOG_FILE=$1

SEQ=$(seq 1 ${_MAX})
for i in ${SEQ};
do
    echo "hello world, iteration: $i" >> ${_LOG_FILE}
    sleep 1
done
